#!/usr/bin/python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Imu
from numpy import fabs

class IMUReconstructor:
    def __init__(self):
        rospy.init_node("imu_velocity_reconstructor")
        self.reconstructed_vel = Twist()
        self.last_vel = Twist()
        self.speed_reconstructed_pub = rospy.Publisher("/imu_speed_reconstructed", Twist, queue_size = 5)
        rospy.Subscriber("/imu/data_raw_transformed", Imu, self.imuCB)
        self.dt = 0.1
        self.offset = [-0.4,.2]
        rospy.spin()

    def imuCB(self, msg):
        #self.reconstructed_vel.linear.x += (msg.linear_acceleration.x - self.last_vel.linear.x) * self.dt
        #self.reconstructed_vel.linear.y += (msg.linear_acceleration.y - self.last_vel.linear.y) * self.dt
        #self.reconstructed_vel.angular.z += (msg.angular_velocity.z - self.last_vel.angular.z) * self.dt
        if msg.linear_acceleration.x > self.offset[0]:
            self.reconstructed_vel.linear.x += (msg.linear_acceleration.x-self.offset[0]) * self.dt
            self.last_vel.linear.x = msg.linear_acceleration.x
        else:
            self.reconstructed_vel.linear.x = 0.0
        if msg.linear_acceleration.y > self.offset[1]:
            self.reconstructed_vel.linear.y += (msg.linear_acceleration.y-self.offset[1 ]) * self.dt
            self.last_vel.linear.y = msg.linear_acceleration.y
        else:
            self.reconstructed_vel.linear.y = 0.0
        self.reconstructed_vel.angular.z = (msg.angular_velocity.z)

        self.speed_reconstructed_pub.publish(self.reconstructed_vel)

        self.last_vel.angular.z = msg.angular_velocity.z
        rospy.loginfo("OK")

IMUReconstructor()
