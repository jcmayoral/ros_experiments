#This could be use for mode based scenarios
#One limitation of the robto might be that everything is running on the background
#Managing launch_files knowing which process runs each of them might potentially
#benefit the computational resources on the robot by the application of modes.
#i.e. while navigating there is no need for manipulation packages.
#This script jus provides de idea throught the launching of the pico_flexx_driver launch,
#however it requires special launch files for the node names might be same if two
#launch files of the same kind and run

import rospy
import rospkg
import roslaunch
import rospy
import threading

class ROSAutomaticLauncher(threading.Thread):
    def __init__(self, ros_pkg = "move_base_flexx", config_folder="ros/launch", launch_file="config.yaml", id="node_n"):
        threading.Thread.__init__(self)
        rospack = rospkg.RosPack()
        file_path = rospack.get_path(ros_pkg)+"/"+config_folder+"/"+launch_file
        rospy.on_shutdown(self.shutdown)
        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        roslaunch.configure_logging(uuid)
        self.launch = roslaunch.parent.ROSLaunchParent(uuid,[file_path])
        self.launch.start()

    def run(self):
        pass

    def shutdown(self):
        rospy.loginfo("Stopping launch file")
        self.destroy() #my own cleaning stuff function, you dont have to have it. but if there is anything you need to do before close the ros node, do it here.
        rospy.sleep(1)

    def destroy(self):
        self.launch.shutdown()

if __name__ == '__main__':
    rospy.init_node('tester_', anonymous=False)
    launch_files = list()
    for i in range(2):
        tmp_launcher = ROSAutomaticLauncher(ros_pkg="pico_flexx_driver", config_folder="launch", launch_file="pico_flexx_nodelet.launch", id=str(i))
        launch_files.append(threading.Thread(target=tmp_launcher.run()))

    for l in launch_files:
        l.start()

    print "dasghdpaisghp"
