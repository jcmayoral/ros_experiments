#!/usr/bin/env python
import tf
import rospy
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from sensor_msgs.msg import Imu
from copy import deepcopy
import threading

class Imu3dPlotter:
    def __init__(self, topic_name, msg_type=Imu):
        #self.fig, self.ax = plt.subplots()
        self.x = 0
        self.y = 0
        self.yaw = 0
        self.is_started = False
        self.clear = False
        self.lock = threading.Lock()
        self.listener = tf.TransformListener()

        #TODO
        if msg_type is Imu:
            print ("IMU")
        else:
            print ("not Imu")
        rospy.Subscriber(topic_name,Imu,self.imu_callback, queue_size = 1)
        rospy.loginfo("PLOT READY")

    def imu_callback(self,msg):
        self.is_started = True
        self.lock.acquire()
        self.x = msg.linear_acceleration.x
        self.y = msg.linear_acceleration.y
        self.yaw = msg.angular_velocity.z
        self.lock.release()


fig = plt.figure()

rospy.init_node("imu3d_plot")
plt.ion()
ax = fig.add_subplot(111, projection='3d')
#plt.subplots_adjust(bottom=0.2)
plot = Imu3dPlotter(topic_name="/imu/data_raw_transformed")
r = 1.5
n = 0
max_samples = 1000

while not rospy.is_shutdown():
    if plot.is_started:
        ax.legend()

        if plot.is_started:
            plot.lock.acquire()
            x1 = deepcopy(plot.x)
            y1= deepcopy(plot.y)
            yaw1= deepcopy(plot.yaw)
            plot.lock.release()
            ax.scatter(x1, y1, yaw1, c='r')
            #for x,y,w in zip(x1, y1, yaw1):
            #ax.arrow(x1, y1,x1+1, y1+2, color='r',  label='odom', head_width=0.025)

        fig.canvas.draw_idle()
        #plt.draw()
        if n > max_samples  :
            ax.cla()
            n=0
            #plt.gcf().clear()
        n+=1
        plt.pause(0.1)
