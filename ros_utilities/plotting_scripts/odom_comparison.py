import sys
import threading
import numpy as np
from copy import deepcopy
from itertools import cycle
from collections import OrderedDict, deque

import rospy
import tf2_ros
import tf2_geometry_msgs
import tf_conversions
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseStamped, Quaternion, Twist

from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout, QTextEdit, QMainWindow, QWidget
from PyQt5.QtGui import QPalette, QFont, QColor
from PyQt5.QtCore import QTimer, Qt

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt

class TFAnalyzer:
    def __init__(self, factor=40,map_frame = "map", odom_frame="odom"):
        self.tf_error= deque(maxlen=25)
        self.map_frame = map_frame
        self.odom_frame = odom_frame
        self.factor = factor
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)
        #self.listener.waitForTransform(map_frame, odom_frame, rospy.Time(0), rospy.Duration(10.0))
        ready = False

        while not ready:

            try:
                transformObject = self.tfBuffer.lookup_transform(map_frame, odom_frame, rospy.Time(0),rospy.Duration(10))
                self.old_pose = transformObject.transform.translation
                self.old_orientation = transformObject.transform.rotation
                ready = True
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                #raise
                print "transform not ready`"



    def run(self):
        #self.listener.waitForTransform(self.map_frame, self.odom_frame, rospy.Time(0), rospy.Duration(1.0))
        transformObject = self.tfBuffer.lookup_transform(self.map_frame, self.odom_frame, rospy.Time(0),rospy.Duration(10))
        p = transformObject.transform.translation
        o = transformObject.transform.rotation
        diff_x = p.x - self.old_pose.x
        diff_y = p.y -self.old_pose.y
        tf_diff = np.power(diff_x,2) + np.power(diff_y,2)
        o_array = [o.x,o.y,o.z,o.w]
        old_o_array = [self.old_orientation.x, self.old_orientation.y, self.old_orientation.z, self.old_orientation.w]
        tf_diff += np.power(tf_conversions.transformations.euler_from_quaternion(o_array)[2] \
                            - tf_conversions.transformations.euler_from_quaternion(old_o_array)[2],2)
        tf_diff = np.sqrt(tf_diff)
        self.tf_error.append(tf_diff*self.factor)
        self.old_pose = p
        self.old_orientation = o


class CmdVelUtil:
    def __init__(self):
        rospy.Subscriber("/cmd_vel", Twist, self.cmd_vel_cb, queue_size = 50)
        self.cmd_vel_x = 0
        self.cmd_vel_z = 0
        self.lock = threading.Lock()

    def cmd_vel_cb(self, msg):
        self.cmd_vel_x = msg.linear.x
        self.cmd_vel_z = msg.angular.z

class OdomPlotter:
    def __init__(self, topic_name):
        #self.fig, self.ax = plt.subplots()
        self.x = 0
        self.y = 0
        self.yaw = 0
        self.q = Quaternion()
        self.twist = Twist()
        self.is_started = False
        self.clear = False
        self.is_initialize = False
        self.lock = threading.Lock()
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)
        rospy.Subscriber(topic_name,Odometry,self.odomCB, queue_size = 100)
        self.topic_source = topic_name
        rospy.loginfo("PLOT READY")

    def odomCB(self,msg):
        self.is_started = True
        odom_pose = PoseStamped()
        self.twist = msg.twist.twist
        odom_pose.header = msg.header
        odom_pose.pose = msg.pose.pose
        explicit_quaternion = [odom_pose.pose.orientation.x, odom_pose.pose.orientation.y, \
                               odom_pose.pose.orientation.z, odom_pose.pose.orientation.w]
        euler =                tf_conversions.transformations.euler_from_quaternion(explicit_quaternion)
        self.lock.acquire()
        self.x = odom_pose.pose.position.x
        self.y = odom_pose.pose.position.y
        self.yaw = euler[2]
        self.q = odom_pose.pose.orientation
        self.lock.release()


class OdomPlotManager(QMainWindow):
    def __init__(self, odom_list, parent=None):

        super(OdomPlotManager, self).__init__(parent)
        self.setStyleSheet("background-color: black;")

        self.fig, self.ax = plt.subplots(2,2)
        self.fig.set_facecolor("black")
        self.fig.set_edgecolor("white")
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setStyleSheet("color: white;")
        self.setWindowFlags(Qt.WindowCloseButtonHint)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.close_button = QPushButton('Close')
        self.reset_button = QPushButton('Reset')
        self.close_request = False
        self.reset_button.setStyleSheet("color: white;")
        self.close_button.setStyleSheet("color: white;")

        self.reset_button.clicked.connect(self.reset)
        self.close_button.clicked.connect(self.close_button_cb)

        self.text_editor = QTextEdit()
        self.text_editor.setReadOnly(True)
        self.text_editor.autoFormatting()

        self.wid = QWidget(self)
        self.setCentralWidget(self.wid)
        self.wid.setGeometry(200, 500, 350, 400) # self.resize(250,250)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)
        self.layout.addWidget(self.close_button)
        self.layout.addWidget(self.reset_button)
        self.layout.addWidget(self.text_editor)

        self.text = ""
        self.wid.setLayout(self.layout)

        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)
        self.odom_sources = list()
        self.last_twist = Twist()
        self.counter_dict = dict()
        self.extrapolation_errors = 0
        self.init_counters()
        self.text_editor.setText("hello")
        self.text_editor.setStyleSheet("color: white;")

        self.timer = QTimer()
        self.timer.timeout.connect(self.write_results)
        self.timer.start(1000)

        self.is_initialize = True
        self.cycolor = cycle('bgrcm')#k')
        for odom_source in odom_list:
            self.odom_sources.append([OdomPlotter(topic_name=odom_source),self.cycolor.next(), self.cycolor.next()])

        self.cmd_fb = CmdVelUtil()
        self.tf_analyzer = TFAnalyzer()
        self.linear_error_list = deque(maxlen=25)
        self.angular_error_list = deque(maxlen=25)
        self.error_list = deque(maxlen=25)

    def init_counters(self):
        self.extrapolation_errors = 0
        self.counter_dict["forward"]=0
        self.counter_dict["forward_left"]=0
        self.counter_dict["forward_right"]=0
        self.counter_dict["backward"]=0
        self.counter_dict["backward_left"]=0
        self.counter_dict["backward_right"]=0
        self.counter_dict["rip_left"]=0
        self.counter_dict["rip_right"]=0
        self.counter_dict["class_error"]=0

    def write_results(self):
        self.text_editor.setText(self.text)
        self.text_editor.scrollToAnchor("Errors")
        self.setFixedSize(self.layout.sizeHint())


    def update_text(self):
        generated_string = "Counted Primitives"
        for k,v in self.counter_dict.iteritems():
            generated_string+="<br>"+k+":"+str(v)
        generated_string+="<br>"+"Extrapolation Errors: " + str(self.extrapolation_errors)
        self.text = generated_string
        # self.textEdit.setText(p1.to_html(col_space = 10, index = False, justify = "right"))


    def close_button_cb(self,event):
        self.close_request = True
        self.close()

    def reset(self,event):
        rospy.loginfo("Reset")
        for a in self.ax:
            for sub in a:
                sub.cla()
                sub.legend()
        self.tfBuffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tfBuffer)
        self.init_counters()

    def update_plot(self,x,y,yaw,r,color,name,index=(0,0),topic_name_extension=""):
        self.ax[index[0],index[1]].scatter(x, y, label=name+topic_name_extension, c=color)
        self.ax[index[0],index[1]].arrow(x, y,r*np.cos(yaw), r*np.sin(yaw), color=color, head_width=0.025)

    def update_my_error_plot(self,name):
        self.ax[1,1].cla()
        #self.ax[3].plot(np.arange(len(self.linear_error_list)),self.linear_error_list, label=name+"_linear_vel_error", c="r")
        self.ax[1,1].plot(np.arange(len(self.tf_analyzer.tf_error)),self.tf_analyzer.tf_error, label=name+"_tf_error", c="b")
        #self.ax[3].plot(np.arange(len(self.angular_error_list)),self.angular_error_list,label=name+"_angular_vel_error",c="g")
        self.ax[1,1].plot(np.arange(len(self.error_list)),self.error_list,label=name+"_vel_error",c="c")

        """
        if len(self.linear_error_list) > 2:
            d_error_diff = np.power(self.linear_error_list[-1] -self.linear_error_list[-2],2)
            d_error_diff += np.power(self.angular_error_list[-1] -self.angular_error_list[-2],2)
            d_error_diff = np.sqrt(d_error_diff)
            self.ax[3].scatter(len(self.linear_error_list),d_error_diff,label=name+"_vel_error_first_derivative",c="b")
            error_diff = np.power(self.linear_error_list[-1],2)
            error_diff += np.power(self.angular_error_list[-1],2)
            error_diff = np.sqrt(error_diff)
            self.ax[3].scatter(len(self.linear_error_list),error_diff,label=name+"_vel_error_first_derivative",c="r")
        """

        self.ax[1,1].legend()

    def plot(self):
        #
        print "plot"
        while not rospy.is_shutdown() and not self.close_request:

            self.cmd_fb.lock.acquire()
            cmd_linear = self.cmd_fb.cmd_vel_x
            cmd_angular = self.cmd_fb.cmd_vel_z
            self.cmd_fb.lock.release()

            if not self.is_initialize:

                for i in self.ax:
                    for j in i:
                        j.set_facecolor('xkcd:black')
                        j.set_fc('xkcd:black')
                        #j.set_xlabel("x", color="white")
                        #j.set_ylabel("y", color="white")
                        j.tick_params(axis='x', colors='white')
                        j.tick_params(axis='y', colors='white')
                        j.legend()

                self.is_initialize = True


            #self.fig.clear()

            r = 1.5

            for source,color,color2 in self.odom_sources:
                if source.is_started:
                    if not source.is_initialize:
                        self.is_initialize = False
                        source.is_initialize = True

                    source.lock.acquire()
                    x1 = deepcopy(source.x)
                    y1= deepcopy(source.y)
                    yaw1= deepcopy(source.yaw)
                    q1 = deepcopy(source.q)
                    twist1 = deepcopy(source.twist)
                    source.lock.release()

                    plot_thread_1 = threading.Thread(target = self.update_plot, args=(x1,y1,yaw1,r,color, source.topic_source,(0,0)))
                    plot_thread_1.start()


                    #test
                    try:
                        p = PoseStamped()
                        p.header.stamp = rospy.Time.now()
                        p.header.frame_id = "/odom"
                        p.pose.position.x = x1
                        p.pose.position.y = y1
                        p.pose.orientation = q1
                        #self.listener.waitForTransform("/map_carto", "/odom", rospy.Time(0),rospy.Duration(1.0))
                        transform = self.tfBuffer.lookup_transform("map", "odom",rospy.Time(0),rospy.Duration(1.0)) #wait for 1 second
                        odom_pose = tf2_geometry_msgs.do_transform_pose(p, transform)
                        explicit_quaternion = [odom_pose.pose.orientation.x, odom_pose.pose.orientation.y, \
                        odom_pose.pose.orientation.z, odom_pose.pose.orientation.w]
                        euler = tf_conversions.transformations.euler_from_quaternion(explicit_quaternion)
                        plot_thread_2 = threading.Thread(target = self.update_plot, args=(odom_pose.pose.position.x,odom_pose.pose.position.y,\
                                                                    euler[2],r,color2, source.topic_source,(0,1),"_map"))
                        plot_thread_2.start()
                    except Exception as e:
                        self.extrapolation_errors+=1
                        print e

                    self.ax[1,0].scatter(twist1.linear.x, twist1.angular.z, label=source.topic_source + "_velocity_space", c=self.cycolor.next())

                    prim_counter = 0
                    prim_class=""
                    tolerance = 0.05

                    if twist1.linear.x > tolerance:
                        if -tolerance < twist1.angular.z < tolerance and twist1.angular.x != 0:
                            self.counter_dict["forward"]+=1
                            prim_counter+=1
                            prim_class+="FW"

                        if twist1.angular.z > tolerance:
                            self.counter_dict["forward_left"]+=1
                            prim_counter+=1
                            prim_class+="FL"


                        if twist1.angular.z < -tolerance:
                            self.counter_dict["forward_right"]+=1
                            prim_counter+=1
                            prim_class+="FR"

                    if twist1.linear.x < -tolerance:
                        if -tolerance < twist1.angular.z < tolerance and twist1.angular.z != 0:
                            self.counter_dict["backward"]+=1
                            prim_counter+=1
                            prim_class+="BW"


                        if twist1.angular.z > tolerance:
                            self.counter_dict["backward_left"]+=1
                            prim_counter+=1
                            prim_class+="BL"

                        if twist1.angular.z < -tolerance:
                            self.counter_dict["backward_right"]+=1
                            prim_counter+=1
                            prim_class+="BR"

                    if -tolerance < twist1.linear.x < tolerance and twist1.linear.x != 0:
                        if twist1.angular.z > tolerance:
                            self.counter_dict["rip_left"]+=1
                            prim_counter+=1
                            prim_class+="RL"

                        if twist1.angular.z < -tolerance:
                            self.counter_dict["rip_right"]+=1
                            prim_counter+=1
                            prim_class+="RR"

                    if prim_counter > 1:
                        print twist1.linear.x, "." , twist1.angular.z
                        print prim_class
                        self.counter_dict["class_error"]+=1

                    #TODO Split plot of different odom sources
                    self.tf_analyzer.run()
                    err_x = cmd_linear - twist1.linear.x
                    err_z = cmd_angular - twist1.angular.z
                    self.linear_error_list.append(err_x)
                    self.angular_error_list.append(err_z)
                    err_magnitude = np.sqrt(np.power(err_x,2) + np.power(err_z,2))
                    self.error_list.append(err_magnitude)

                    plot_thread_4 = threading.Thread(target = self.update_my_error_plot, args=(source.topic_source,))
                    plot_thread_4.start()


                    self.last_twist = twist1
                    self.update_text()
                    self.canvas.draw()

sources_list = ['/bag_odom', '/odom', '/fusion_odom']

rospy.init_node("odom_comparison_plot")

app = QApplication(sys.argv)
odom_manager = OdomPlotManager(sources_list)
plot_thread = threading.Thread(target = odom_manager.plot)
plot_thread.start()
odom_manager.show()
sys.exit(app.exec_())
