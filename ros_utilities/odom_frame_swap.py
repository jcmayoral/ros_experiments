import rospy
from nav_msgs.msg import Odometry



class FrameChanger:
    def __init__(self):
        rospy.init_node("odom_frame_changer")
        self.p = rospy.Publisher("new_odom", Odometry, queue_size=1)
        self.s = rospy.Subscriber("/odom", Odometry, self.odom_cb)
        rospy.spin()

    def odom_cb(self,msg):
        msg.header.frame_id = "fake_odom"
        self.p.publish(msg)


FrameChanger()
