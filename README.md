magazino_fdd an attempt to create a fault detection node using the information provided by topics in order to track the component which is faulty. Requires a lot of time to do it properly but can be very useful in the future to track fault components (if properly finished).

multi_stage provides launch files to use multi_robot_comm package on stage with multiple robots... a server negotiates the robot which will go to a goal destination by "TIME OF EXECUTION". Very first steps.

odometry_fusion param files and launch files on an attempt to fuse the imu with the wheel odom in order to optimize the robot odometry.


ros_utilities just provides a very simple fake_imu node and several real time plotting scripts
