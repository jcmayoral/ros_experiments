/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   recovery_executor.h
 * Author: banos
 *
 * Created on November 16, 2018, 11:28 AM
 */

#ifndef RECOVERY_EXECUTOR_H
#define RECOVERY_EXECUTOR_H

#include <yaml-cpp/yaml.h>
#include <iostream>
#include <string>

namespace magazino_fdd{
    class RecoveryExecutor {
    public:
        RecoveryExecutor(YAML::Node node);
        RecoveryExecutor(const RecoveryExecutor& orig, YAML::Node node) {
        }
        virtual ~RecoveryExecutor();
        std::string getRecoveryStrategy(std::string key);
    private:
        std::map<std::string, std::string> strategy_selector_;
    };
};
#endif /* RECOVERY_EXECUTOR_H */

