/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   recovery_executor.cpp
 * Author: banos
 * 
 * Created on November 16, 2018, 11:28 AM
 */

#include <magazino_fdd/recovery_executor.h>

using namespace magazino_fdd;

RecoveryExecutor::RecoveryExecutor(YAML::Node node) {
    std::cout << "RE Constructor" << std::endl;
    for (YAML::const_iterator a= node.begin(); a != node.end(); ++a){
        std::string name = a->first.as<std::string>();
        YAML::Node config = a->second;
        strategy_selector_[name] = config["strategy"].as<std::string>();
    }
}

std::string RecoveryExecutor::getRecoveryStrategy(std::string key){
    if (strategy_selector_[key].empty()){
        return "Unknown Error";
    }
    return strategy_selector_[key];    
}


RecoveryExecutor::~RecoveryExecutor() {
}

